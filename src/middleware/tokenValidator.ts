import { NextFunction, Request, Response } from "express";
import { ExpressMiddlewareInterface } from "routing-controllers";
import Container from "typedi";
import { UserService } from "../api/users/users.service";
import { ITokenPayload } from "../common/common-types";
import { decodeToken } from "../utils/common/jwt";

export class TokenValidator implements ExpressMiddlewareInterface {
  protected _userService = Container.get(UserService);
  async use(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    const accessToken = req.headers.authorization;
    if (!accessToken) {
      return res.status(401).send({
        status: false,
        error: {
          code: 'ATH-001',
          message: 'Provide authentication token!'
        }
      });
    }
    const decodedData: ITokenPayload = await decodeToken(accessToken);
    if (!decodedData.id) {
      return res.status(401).send({
        status: false,
        error: {
          code: 'ATH-001',
          message: 'Provide authentication token!'
        }
      });
    }
    const { email, id } = decodedData,
      options = {
        select: ['id', 'email', 'firstName']
      }
    const user = await this._userService.findUser({email, id}, options);
    Object.assign(req, {
      context: { user }
    });
    next();
  }
}