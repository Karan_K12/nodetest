import { MYSQL_CONFIG } from '../../config';
import { createConnection, Connection } from 'typeorm';
import { User } from '../../api/users/users.model';

class MySqlConnector {
  private static mySqlConnection: Connection;

  get connection(): Connection {
    return MySqlConnector.mySqlConnection;
  }

  public async connect(): Promise<any> {
    // tslint:disable-next-line:no-console
    try {
      await createConnection({
        ...MYSQL_CONFIG,
        type: 'mysql',
        entities: [ User ],
        migrations: [],
        synchronize: true,
        logging: false,
      });
      console.log('connected to mysql');
    } catch (err) {
      console.log('Error in sql connection:', err);
    }
  }

  /**
   * Disconnects from MySQL
   * @returns {Promise<any>}
   */
  public disconnect(): Promise<any> {
    return MySqlConnector.mySqlConnection.close();
  }
}

export default new MySqlConnector();
