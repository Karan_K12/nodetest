import { MONGO_URL, MONGO_CONFIG } from '../../config';
import mongoose from 'mongoose';

const connectMongoDB = async () => {
  try {
    const options: mongoose.ConnectionOptions = {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
      poolSize: MONGO_CONFIG.poolSize,
      family: 4
    };
    mongoose.connect(MONGO_URL, options);

    // mongoose.set('debug', true);

    const mongoDb = mongoose.connection;
    mongoDb.on('error', () => {
      console.log(`Unable to connect to mongo database`);
    });

    mongoDb.once('open', () => {
      console.log(`Connected to mongo database`);
    });
  } catch (err) {
    console.error('error:', err);
  }
};

export default connectMongoDB;
