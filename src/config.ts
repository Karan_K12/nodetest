require('dotenv').config();
export const PORT = process.env.PORT || 3000;
export const MONGO_URL = process.env.MONGO_URL || '';
export const MONGO_CONFIG = {
  poolSize: parseInt(process.env.MONGO_POOL_SIZE || '') || 5
};
export const MYSQL_CONFIG = {
  url: `${process.env.MYSQL_URL}/${process.env.MYSQL_DBNAME}` || ''
};
export const MYSQL_DBNAME = process.env.MYSQL_DBNAME || '';
export const JWT_SECRET = process.env.JWT_SECRET || '';