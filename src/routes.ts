import { Application } from "express";
import { useExpressServer } from "routing-controllers";
import CategoryController from "./api/category/category.controller";
import ContentController from "./api/content/content.controller";
import UserController from "./api/users/users.controller";

const basePath = '/api/v1';
function initRoute(app: Application) {
  useExpressServer(app, {
    controllers: [
      UserController,
      CategoryController,
      ContentController
    ],
    defaultErrorHandler: true,
    routePrefix: basePath
  });
}

export default initRoute;