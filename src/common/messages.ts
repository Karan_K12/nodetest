export const messages: {[key: string]: any} = {
  success: {
    'SGN-001': 'User sign-up successfully!',
    'SGN-002': 'Logged in successfully!',
    'CET-001': 'Category created!',
    'CET-002': 'Listing successful!',
    'CNT-001': 'Content created!'
  },
  error: {
    'SGN-001': 'Error while sign-up!',
    'SGN-002': 'E-mail address not found!',
    'SGN-003': 'Incorrect password!',
    'CET-001': 'Error while creating category!',
    'CET-002': 'Error in listing!',
    'CNT-001': 'Error while creating content!'
  }
}