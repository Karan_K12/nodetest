import * as mongoose from 'mongoose';
import { ICategory } from "./category.interface";

const categorySchema: mongoose.Schema<ICategory> = new mongoose.Schema({
  name: { type: String, required: true },
  createdBy: { type: Number, required: true },
  localCreatedAt: { type: Number, default: Date.now() }
}, {
  timestamps: true
})

categorySchema.index({ name: 1 });
const categoryModel = mongoose.model<ICategory>('category', categorySchema);
categoryModel.ensureIndexes();

export default categoryModel;