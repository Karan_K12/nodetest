import { Body, Get, JsonController, Post, QueryParams, Req, Res, UseBefore } from "routing-controllers";
import Container from "typedi";
import { IExtendedRequest, IExtendedResponse } from "../../common/common-types";
import { TokenValidator } from "../../middleware/tokenValidator";
import { CategoryService } from "./category.service";
import { CreateCategoryValidator } from "./dto/category.create.dto";
import { ListCategory } from "./dto/category.list.dto";

@JsonController('/category')
@UseBefore(TokenValidator)
export default class CategoryController {
  protected _categoryService: CategoryService;

  constructor() {
    this._categoryService = Container.get(CategoryService);
  }

  @Post('/create', { transformRequest: true })
  async createCategory(
    @Req() req: IExtendedRequest,
    @Res() res: IExtendedResponse,
    @Body() body: CreateCategoryValidator
  ) {
    try {
      const { name } = body.payload,
      userId = req.context?.user?.id,
      item = {
        name,
        createdBy: userId,
        localCreatedAt: Date.now()
      },
      category = await this._categoryService.save(item);
      return res.formatter.ok(category, true, 'CET-001');
    } catch (err) {
      return res.formatter.error(null, false, 'CET-001', new Error('Error!'));
    }
  }

  @Get('/list', { transformRequest: true })
  async listCategories(
    @Req() req: IExtendedRequest,
    @Res() res: IExtendedResponse,
    @QueryParams() query: ListCategory
  ) {
    try {
      const { currentPage = 0, limit = 20 } = query,
        options = {
          lean: true,
          skip: currentPage * limit,
          limit
        },
        [data, count] = await Promise.all([this._categoryService.find({}, {}, options), this._categoryService.count({})]),
        response = {
          list: data,
          totalPages: Math.ceil(count / limit),
          count
        };

        return res.formatter.ok(response, true, 'CET-002');
    } catch (err) {
      return res.formatter.error(null, false, 'CET-002', new Error('Error'));
    }
  }
}