import { IsNumber, IsOptional } from "class-validator";

export class ListCategory {
  @IsNumber()
  @IsOptional()
  currentPage: number;

  @IsNumber()
  @IsOptional()
  limit: number;
}