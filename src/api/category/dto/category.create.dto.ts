import { Type } from "class-transformer";
import { IsDefined, IsObject, IsString, ValidateNested } from "class-validator";

export class CreateCategory {
  @IsString()
  @IsDefined()
  name: string;
}

export class CreateCategoryValidator {
  @ValidateNested({ each: true })
  @IsObject()
  @Type(() => CreateCategory)
  payload: CreateCategory;
}