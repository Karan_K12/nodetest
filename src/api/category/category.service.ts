import { FilterQuery, Model, QueryOptions } from "mongoose";
import { Service } from "typedi";
import { ICategory } from "./category.interface";
import categoryModel from "./category.model";

@Service()
export class CategoryService {
  private model: Model<ICategory>
  constructor() {
    this.model = categoryModel;
  }

  async save(item: any): Promise<any> {
    try {
      return await this.model.create(item);
    } catch (err) {
      return err;
    }
  }

  async find(filter: FilterQuery<ICategory>, project: any, options: QueryOptions = { lean: true }) {
    try {
      return await this.model.find(filter, project, options).lean();
    } catch (err) {
      return err;
    }
  }

  async count(filter: FilterQuery<ICategory>): Promise<number> {
    return await this.model.countDocuments(filter);
  }
}