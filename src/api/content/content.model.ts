import * as mongoose from 'mongoose';
import { IContent } from "./content.interface";

const contentSchema: mongoose.Schema<IContent> = new mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String },
  categoryId: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'category' },
  createdBy: { type: Number, required: true },
  localCreatedAt: { type: Number, default: Date.now() }
}, {
  timestamps: true
})

contentSchema.index({ categoryId: 1 });
const contentModel = mongoose.model<IContent>('content', contentSchema);
contentModel.ensureIndexes();

export default contentModel;