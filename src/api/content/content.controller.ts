import { Body, Get, JsonController, Post, QueryParams, Req, Res, UseBefore } from "routing-controllers";
import Container from "typedi";
import { IExtendedRequest, IExtendedResponse } from "../../common/common-types";
import { TokenValidator } from "../../middleware/tokenValidator";
import { ContentService } from "./content.service";
import Mongoose from 'mongoose';
import { CreateContentValidator } from "./dto/content.create.dto";
import { ListCategory } from "./dto/content.list.dto";

@JsonController('/content')
@UseBefore(TokenValidator)
export default class ContentController {
  protected _contentService: ContentService;

  constructor() {
    this._contentService = Container.get(ContentService);
  }

  @Post('/create', { transformRequest: true })
  async createCategory(
    @Req() req: IExtendedRequest,
    @Res() res: IExtendedResponse,
    @Body() body: CreateContentValidator
  ) {
    try {
      const { name, description = '', categoryId } = body.payload,
      userId = req.context?.user?.id,
      item = {
        name,
        description,
        categoryId,
        createdBy: userId,
        localCreatedAt: Date.now()
      },
      category = await this._contentService.save(item);
      return res.formatter.ok(category, true, 'CNT-001');
    } catch (err) {
      return res.formatter.error(null, false, 'CNT-001', new Error('Error!'));
    }
  }

  @Get('/list', { transformRequest: true })
  async listCategories(
    @Req() req: IExtendedRequest,
    @Res() res: IExtendedResponse,
    @QueryParams() query: ListCategory
  ) {
    try {
      const { currentPage = 0, limit = 20, categoryId } = query,
        queryOpt = {
          categoryId: Mongoose.Types.ObjectId(categoryId)
        },
        pipeLine =[
          { $match: queryOpt },
          {
            $lookup: {
              from: 'categories',
              localField: 'categoryId',
              foreignField: '_id',
              as: 'categories'
            }
          },
          { $unwind: '$categories' },
          { $project: {
            _id: 1,
            name: 1,
            description: 1,
            categoryId: 1,
            createdBy: 1,
            categoryName: '$categories.name'
          }},
          {
            $facet: {
              paginatedResults: [
                { $skip: limit * currentPage },
                { $limit: limit }
              ],
              totalCount: [{ $count: 'count' }]
            }
          }
        ],
        data = await this._contentService.aggregate(pipeLine),
        count = data.length ? data[0]?.totalCount[0]?.count : 0,
        response = {
          list: data.length ? data[0].paginatedResults : [],
          totalPages: Math.ceil(count / limit),
          count
        };

        return res.formatter.ok(response, true, 'CET-002');
    } catch (err) {
      return res.formatter.error(null, false, 'CET-002', new Error('Error'));
    }
  }
}