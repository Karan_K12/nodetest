import { IsDefined, IsNumber, IsOptional, IsString } from "class-validator";

export class ListCategory {
  @IsString()
  @IsDefined()
  categoryId: string

  @IsNumber()
  @IsOptional()
  currentPage: number;

  @IsNumber()
  @IsOptional()
  limit: number;
}