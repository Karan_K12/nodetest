import { Type } from "class-transformer";
import { IsDefined, IsObject, IsOptional, IsString, ValidateNested } from "class-validator";

export class CreateContent {
  @IsString()
  @IsDefined()
  name: string;

  @IsString()
  @IsOptional()
  description: string;

  @IsString()
  @IsDefined()
  categoryId: string;
}

export class CreateContentValidator {
  @ValidateNested({ each: true })
  @IsObject()
  @Type(() => CreateContent)
  payload: CreateContent;
}