import { FilterQuery, Model, QueryOptions } from "mongoose";
import { Service } from "typedi";
import { IContent } from "./content.interface";
import contentModel from "./content.model";

@Service()
export class ContentService {
  private model: Model<IContent>
  constructor() {
    this.model = contentModel;
  }

  async save(item: any): Promise<any> {
    try {
      return await this.model.create(item);
    } catch (err) {
      return err;
    }
  }

  async find(filter: FilterQuery<IContent>, project: any, options: QueryOptions = { lean: true }) {
    try {
      return await this.model.find(filter, project, options).lean();
    } catch (err) {
      return err;
    }
  }

  async aggregate(pipeLine:any[]) : Promise<any> {
    try {
      return await this.model.aggregate(pipeLine);
    } catch (err) {
      return err;
    }
  }

  async count(filter: FilterQuery<IContent>): Promise<number> {
    return await this.model.countDocuments(filter);
  }
}