import { Document, Mongoose } from "mongoose";
import { ICategory } from "../category/category.interface";

export interface IContent extends Document {
  name: string;
  description: string;
  categoryId: ICategory['_id'];
  createdBy: number;
  localCreatedAt: number;
}