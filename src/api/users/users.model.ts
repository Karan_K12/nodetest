import {
  Column,
  Entity,
  Index,
  PrimaryGeneratedColumn
} from 'typeorm';
import { MYSQL_DBNAME } from '../../config';

@Index('email', ['email'], { fulltext: true })
@Entity('user', { schema: MYSQL_DBNAME })

export class User {
  @PrimaryGeneratedColumn({ type: 'int', name: 'id' })
  id: number;

  @Column('varchar', { name: 'email', length: 150 })
  email: string;

  @Column('mediumtext', { name: 'password' })
  password: string;

  @Column('mediumtext', { name: 'first_name' })
  firstName: string;

  @Column('mediumtext', { name: 'last_name', default: null })
  lastName: string;

  @Column('varchar', { name: 'gender', length: 10, default: null })
  gender: string;

  @Column('mediumtext', { name: 'user_role' })
  userRole: string;

  @Column('mediumtext', { name: 'designation', default: null })
  designation: string;

  @Column('mediumtext', { name: 'organisation', default: null })
  organisation: string;

  @Column('varchar', { name: 'phone', length: 20, default: null })
  phone: string;

  @Column('varchar', { name: 'city', length: 100, default: null })
  city: string;

  @Column('varchar', { name: 'state', length: 100, default: null })
  state: string;

  @Column('varchar', { name: 'country', length: 100 })
  country: string;
}