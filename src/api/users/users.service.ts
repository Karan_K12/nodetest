import { Service } from "typedi";
import { FindConditions, FindOneOptions, Repository } from "typeorm";
import { InjectRepository } from "typeorm-typedi-extensions";
import { User } from "./users.model";

@Service()
export class UserService {
  constructor(
    @InjectRepository(User)
    private _userRepo: Repository<User>
  ) {
  }

  async createUser(item: any) {
    try {
      return await this._userRepo.save(item);
    } catch (err) {
      return err;
    }
  }

  async findUser(condition: FindConditions<User>, options?: FindOneOptions): Promise<any> {
    try {
      return await this._userRepo.findOne(condition, options);
    } catch (err) {
      return err;
    }
  }
}