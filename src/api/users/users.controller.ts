import { Request } from "express";
import { Body, JsonController, Post, Req, Res } from "routing-controllers";
import Container from "typedi";
import { IExtendedResponse, ITokenPayload } from "../../common/common-types";
import { createToken } from "../../utils/common/jwt";
import { encryptPwd } from "../../utils/common/password";
import { LoginValidator } from "./dto/users.login.dto";
import { SignUpValidator } from "./dto/users.signup.dto";
import { UserService } from "./users.service";

@JsonController('/user')
export default class UserController {
  protected _userService: UserService;

  constructor() {
    this._userService = Container.get(UserService);
  }

  @Post('/signup', { transformRequest: true })
  async userSignup(
    @Res() res: IExtendedResponse,
    @Body() body: SignUpValidator
  ) {
    try {
      const { email, password } = body.payload;
      // check if the user already exists or not
      const user = await this._userService.findUser({email});
      if (user)
        return res.formatter.error(null, false, 'SGN-001', new Error('User already exists'));

      const encrypted = encryptPwd(password),
        item = body.payload;
      item.password = encrypted;
      const newUser = await this._userService.createUser(item);
      return res.formatter.ok(newUser, true, 'SGN-001');
    } catch (err) {
      return res.formatter.error(null, false, 'SGN-001', new Error('Error'));
    }
  }

  @Post('/login', { transformRequest: true })
  async userLogin(
    @Req() req: Request,
    @Res() res: IExtendedResponse,
    @Body() body: LoginValidator
  ) {
    try {
      const { email, password } = body.payload,
        options = {
          select: ['id', 'email', 'password', 'userRole']
        },
        user = await this._userService.findUser({email}, options);

      // check if the user exists or not
      if (!user)
        return res.formatter.error(null, false, 'SGN-002', new Error('Login failed!'));

      // compare the password
      const encrypted = encryptPwd(password);
      if (user.password !== encrypted)
        return res.formatter.error(null, false, 'SGN-003', new Error('Login failed!'));

      // generate the jwt token as login is successful
      const payload: ITokenPayload = {
        email: user.email,
        id: user.id,
        role: user.userRole
      },
      token = await createToken(payload),
      response = {
        token
      }
      return res.formatter.ok(response, true, 'SGN-002');
    } catch (err) {
      return res.formatter.error(null, false, 'SGN-002', new Error('Error!'));
    }
  }
}