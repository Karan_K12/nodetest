import { Type } from "class-transformer";
import { IsDefined, IsNumber, IsObject, IsOptional, IsString, ValidateNested } from "class-validator";

export class SignUpBody {
  @IsString()
  @IsDefined()
  email: string;

  @IsDefined()
  password: string;

  @IsString()
  @IsDefined()
  firstName: string;

  @IsString()
  @IsOptional()
  lastName: string;

  @IsString()
  @IsDefined()
  gender: string;

  @IsString()
  @IsDefined()
  userRole: string;

  @IsString()
  @IsOptional()
  designation: string;

  @IsString()
  @IsOptional()
  organisation: string;

  @IsNumber()
  @IsOptional()
  phone: number;

  @IsString()
  @IsOptional()
  city: string;

  @IsString()
  @IsOptional()
  state: string;

  @IsString()
  @IsDefined()
  country: string;
}

export class SignUpValidator {
  @ValidateNested({ each: true })
  @IsObject()
  @Type(() => SignUpBody)
  payload: SignUpBody;
}