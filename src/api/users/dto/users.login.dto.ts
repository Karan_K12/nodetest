import { Type } from "class-transformer";
import { IsDefined, IsObject, IsString, ValidateNested } from "class-validator";

export class LoginBody {
  @IsString()
  @IsDefined()
  email: string;

  @IsDefined()
  password: string;
}

export class LoginValidator {
  @ValidateNested({ each: true })
  @IsObject()
  @Type(() => LoginBody)
  payload: LoginBody;
}