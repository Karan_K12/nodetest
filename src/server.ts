import 'reflect-metadata';
import express from 'express';
import { json } from 'body-parser';
import cors from 'cors';
import initRoute from './routes';
import connectMongoDB from './utils/db/mongo.db';
import mysqlConnector from './utils/db/mysql.connector';
import { useContainer } from 'typeorm';
import { Container } from 'typeorm-typedi-extensions';
import { responseFormatter } from './middleware/responseFormattor';

class Server {
  public app: express.Application = express();
  
  constructor() {
    this.app.use(json());
    this.app.use(cors());
    
    // add custom middleware
    this.app.use(responseFormatter());

    initRoute(this.app);

    this.config();
  }

  private config() {
    useContainer(Container);
    connectMongoDB();
    mysqlConnector.connect()
  }
}

export default new Server().app;